package mpjp.shared.geom;

import java.io.Serializable;

public class LineTo extends Object implements Segment, Serializable{
	static final long serialVersionUID = 1L;
	Point endPoint;
	
	public LineTo() {
		
	}

	public LineTo(Point endPoint) {
		this.endPoint = endPoint;
	}

	public Point getEndPoint() {
		return endPoint;
	}
	
	public void setEndPoint(Point endPoint) {
		this.endPoint = endPoint;
	}
		
}
