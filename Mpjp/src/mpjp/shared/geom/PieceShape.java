package mpjp.shared.geom;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class PieceShape extends Object implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Segment> segments;
	Point startPoint;
	
	public PieceShape() {
		this.startPoint = new Point();
		this.segments = new LinkedList<Segment>();
	}
	
	public PieceShape(Point startPoint) {
		this.startPoint = startPoint;
		this.segments = new LinkedList<Segment>();
	}
	
	public PieceShape(Point startPoint, List<Segment> segments) {
		this.startPoint = startPoint;
		this.setSegments(segments);
	}
	
	public Point getStartPoint() {
		return startPoint;
	}

	public List<Segment> getSegments() {
		return segments;
	}

	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
	}

	public void setSegments(List<Segment> segments) {
		this.segments = segments;
	}
	
	public PieceShape addSegment​(Segment segment) {
		segments.add(segment);
		return this;
	}
}
