package mpjp.shared;
import java.io.Serializable;

import mpjp.shared.geom.Point;

public class PieceStatus extends Object implements HasPoint, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int block;
	private int id;
	private Point position;
	private double rotation;
	
	public PieceStatus() {
		this.setBlock(0);
		this.setId(0);
		this.setPosition(new Point(0,0));
		this.setRotation(0);
	}
	
	public PieceStatus(int id, Point position) {
		this.setBlock(0);
		this.setId(id);
		this.setPosition(position);
		this.setRotation(0);
	}

	public int getBlock() {
		return block;
	}

	public void setBlock(int block) {
		this.block = block;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Point getPosition() {
		return position;
	}
	
	public double getX() {
		return position.getX();
	}

	public double getY() {
		return position.getY();
	}
	
	public void setPosition(Point position) {
		this.position = position;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}
}
