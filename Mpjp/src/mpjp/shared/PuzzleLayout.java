package mpjp.shared;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PuzzleLayout implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Map<Integer,List<Integer>> blocks;
	Map<Integer,PieceStatus> pieces;
	int percentageSolved;
	
	public PuzzleLayout() {
		this.blocks = new HashMap<Integer, List<Integer>>();
		this.pieces = new HashMap<Integer, PieceStatus>();
		this.percentageSolved = 0;
	}
	
	public PuzzleLayout(Map<Integer, PieceStatus> pieces, Map<Integer, List<Integer>> blocks, int percentageSolved) {
		this.blocks = blocks;
		this.pieces = pieces;
		this.percentageSolved = percentageSolved;
	}

	public Map<Integer, List<Integer>> getBlocks() {
		return blocks;
	}

	public int getPercentageSolved() {
		return percentageSolved;
	}

	public Map<Integer, PieceStatus> getPieces() {
		return pieces;
	}
	
	public boolean isSolved() {
		if (percentageSolved == 100 || blocks.size() == 1) {
			return true;
		}
		return false;
	}
}