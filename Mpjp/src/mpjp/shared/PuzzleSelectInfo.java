package mpjp.shared;
import java.util.Date;

public class PuzzleSelectInfo extends PuzzleInfo {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int percentageSolved;
	Date start;
	
	public PuzzleSelectInfo(PuzzleInfo info, Date start, int percentageSolved) {
		this.percentageSolved = percentageSolved;
		this.start = start;
		info = new PuzzleInfo();
	}

	public int getPercentageSolved() {
		return percentageSolved;
	}

	public Date getStart() {
		return start;
	}

}
