package mpjp.shared;

import java.util.Date;
import java.util.Map;

import mpjp.shared.geom.PieceShape;
import mpjp.shared.geom.Point;

public class PuzzleView extends Object {
	String image;
	Map<Integer,Point> locations;
	double pieceHeight , pieceWidth , puzzleHeight , puzzleWidth , workspaceHeight , workspaceWidth;
	Map<Integer,PieceShape> shapes;
	Date start;
	
	
	public PuzzleView(Date start, double workspaceWidth, double workspaceHeight, double puzzleWidth, double puzzleHeight,
					  double pieceWidth, double pieceHeight, String image, Map<Integer,PieceShape> shapes, Map<Integer,Point> locations) {

		this.image = image;
		this.locations = locations;
		this.pieceHeight = pieceHeight;
		this.pieceWidth = pieceWidth;
		this.puzzleHeight = puzzleHeight;
		this.puzzleWidth = puzzleWidth;
		this.workspaceHeight = workspaceHeight;
		this.workspaceWidth = workspaceWidth;
		this.shapes = shapes;
		this.start = start;
	}

	public Date getStart(){
		return start;
	}
	
	public double getWorkspaceWidth() {
		return workspaceWidth;
	}
	
	public double getWorkspaceHeight() {
		return workspaceHeight;
	}
	
	public double getPuzzleWidth() {
		return puzzleWidth;
	}
	
	public double getPuzzleHeight() {
		return puzzleHeight;
	}
	
	public double getPieceWidth() {
		return pieceWidth;
	}
	
	public double getPieceHeight() {
		return pieceHeight;
	}
	
	public String getImage(){
		return image;
	}
	
	public PieceShape getPieceShape(int id) {
		return shapes.get(id);
	}
	
	public Point getStandardPieceLocation(int id) {
		return locations.get(id);
	}
	
}
