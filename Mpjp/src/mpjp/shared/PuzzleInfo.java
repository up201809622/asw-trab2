package mpjp.shared;

import java.io.Serializable;

public class PuzzleInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String imageName;
	private String cuttingName;
	private int rows;
	private int columns;
	private double width;
	private double height;
	
	public PuzzleInfo() {}
	
	public PuzzleInfo(String imageName, String cuttingName, int rows, int columns, double width, double height) {
		this.setImageName(imageName);
		this.setCuttingName(cuttingName);
		this.setRows(rows);
		this.setColumns(columns);
		this.setWidth(width);
		this.setHeight(height);
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getCuttingName() {
		return cuttingName;
	}

	public void setCuttingName(String cuttingName) {
		this.cuttingName = cuttingName;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

}
