package mpjp.shared;

public class MPJPException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MPJPException() {}
	
	public MPJPException(String msg) {
		super(msg);
	}

	public MPJPException(String msg, Throwable cause) {
		super(msg,cause);
	}
	
	public MPJPException(String msg, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(msg, cause);
	}
	
	public MPJPException(Throwable cause) {
		super(cause);
	}
}
