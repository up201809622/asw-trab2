package mpjp.quad;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import mpjp.shared.HasPoint;

public class PointQuadtree<T extends HasPoint> implements Iterable<T> {
	Trie<T> top;
	double width;
	double height;
	double margin;
	double topLeftX; 
	double topLeftY; 
	double bottomRightX; 
	double bottomRightY;
	
	/**
	 * Constructor
	 * @param width
	 * @param height
	 */
	public PointQuadtree(double width, double height) {
		this.topLeftX = 0;
		this.topLeftY = height;
		this.bottomRightX = width;
		this.bottomRightY = 0;
		
		this.width = width;
		this.height = height;
		top = new NodeTrie<T>(bottomRightX, bottomRightY, topLeftX, topLeftY);
	}

	/**
	 * Constructor
	 * @param width
	 * @param height
	 * @param margin
	 */
	public PointQuadtree(double width, double height, double margin) {
		this.topLeftX = -margin;
		this.topLeftY = height+margin;
		this.bottomRightX = width+margin;
		this.bottomRightY = -margin;
		
		this.width = width;
		this.height = height;
		this.margin = margin;
		top = new NodeTrie<T>(bottomRightX, bottomRightY, topLeftX, topLeftY);
	}

	/**
	 * Constructor
	 * @param topLeftX
	 * @param topLeftY
	 * @param bottomRightX
	 * @param bottomRightY
	 */
	public PointQuadtree(double topLeftX, double topLeftY, double bottomRightX, double bottomRightY) {
		this.topLeftX = topLeftX;
		this.topLeftY = topLeftY;
		this.bottomRightX = bottomRightX;
		this.bottomRightY = bottomRightY;
		top = new NodeTrie<T>(bottomRightX, bottomRightY, topLeftX, topLeftY);
	}

	class PointIterator implements Iterator<T>, Runnable, Visitor<T> {
		T nextPoint;
		boolean terminated;
		Thread thread;
		
		PointIterator() {
			thread = new Thread(this, "Point Iterator");
			thread.start();
		}
		
		public void run() {
			terminated = false;
			visit((NodeTrie<T>) top);
			synchronized(this) {
				terminated = true;
				handshake();
			}
		}
		
		public boolean hasNext() {
			synchronized(this) {
				if (!terminated) {
					handshake();
				}
			}
			return nextPoint != null;
		}
		
		public T next() {
			T res = nextPoint;
			synchronized(this) { nextPoint = null; }
			return res;
		}
		
		public void visit(LeafTrie<T> leaf) {
			// leaf.accept(this);
			
			for (T p : leaf.getPoints()) {
				synchronized(this) {
					if (nextPoint != null) {
						handshake();
					}
					nextPoint = p;
					handshake();
				}
			}
		}
		
		public void visit(NodeTrie<T> node) {
			// TODO: visit (Node)
			for (Trie<T> trie : node.getTries()) {
				trie.accept(this);
			}
		}
		
		private void handshake() {
			notify();
			try {
				wait();
			} catch (InterruptedException e) {
				throw new RuntimeException("Unexpected interruption while waiting", e);
			}
		}
	}	
	
	public T find(T point) {
		return top.find​(point);
	}
	
	public void insert(T point) {
		if (point.getX() > bottomRightX || point.getX() < topLeftX || 
			point.getY() < bottomRightY || point.getY() > topLeftY) {
			throw new PointOutOfBoundException();
		}
		top.insert(point);
	}
	
	public void insertReplace(T point) {
		top.insertReplace(point);
	}
	
	public Set<T> findNear(double x, double y, double radius) {
		Set<T> nodes = new HashSet<T>();
		top.collectNear(x,y,radius,nodes);
		return nodes;	
	}
	
	public void delete(T point) {
		top.delete(point);
	}
	
	public Set<T> getAll() {
		Set<T> nodes = new HashSet<T>();
		top.collectAll(nodes);
		return nodes;
	}

	@Override
	public Iterator<T> iterator() {
		return new PointIterator();
	}
}
