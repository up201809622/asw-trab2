package mpjp.quad;

import java.util.Set;

import mpjp.shared.HasPoint;

public abstract class Trie<T extends HasPoint> implements Element<T> {
	double bottomRightX;
	double bottomRightY;
	double topLeftX;
	double topLeftY;
	static int capacity;
	
	static enum Quadrant {
		NW,
		NE,
		SE,
		SW;
		
		public static Trie.Quadrant valueOf​(String name) throws IllegalArgumentException, NullPointerException {
			// TODO: valueOf
			return null;
		}
	}

	public Trie(double bottomRightX, double bottomRightY, double topLeftX, double topLeftY) {
		this.bottomRightX = bottomRightX;
		this.bottomRightY = bottomRightY;
		this.topLeftX = topLeftX;
		this.topLeftY = topLeftY;
	}
	
	/**
	 * Get capacity of a bucket
	 * @return capacity
	 */
	public static int getCapacity() { return capacity; }
	/**
	 * Set capacity of a bucket
	 * @param capacity - of bucket
	 */
	public static void setCapacity​(int capacity_) { capacity = capacity_; }

	/**
	 * Find a recorded point with the same coordinates of given point
	 * @param point - with requested coordinates
	 * @return recorded point, if found; null otherwise
	 */
	abstract T find​(T point);
	abstract Trie<T> insert(T point);
	
	/**
	 * Insert given point
	 * @param point - to be inserted
	 * @return changed parent node
	 */
	abstract Trie<T> insertReplace(T point);
	
	/**
	 * Collect points at a distance smaller or equal to radius from (x,y) and place them in given list
	 * @param x
	 * @param y
	 * @param radius
	 * @param points
	 * @return changed parent node
	 */
	abstract void collectNear(double x, double y, double radius, Set<T> points);
	
	/**
	 * Collect all points in this node and its descendants in given set
	 * @param points
	 */
	abstract void collectAll(Set<T> points);
	
	/**
	 * Delete given point
	 * @param point
	 */
	abstract void delete(T point);
	
	/**
	 * Check if overlaps with given circle
	 * @param x
	 * @param y
	 * @param radius
	 * @return true if overlaps and false otherwise
	 */
	boolean overlaps​(double x, double y, double radius) {
		// TODO: overlaps
		// if the distance between the centres of the circles is less than 
		// the sum of the radius of the two circles then they are overlapping.
		
		double centerX = bottomRightX+topLeftX/2;
		double centerY = bottomRightY+topLeftY/2;
		double centerRad = getDistance​(centerX,centerY,topLeftX,topLeftY);
		
		if (getDistance​(centerX,centerY,x,y) < radius+centerRad) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Euclidean distance between two pair of coordinates of two points
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return distance between given points
	 */
	public static double getDistance​(double x1,double y1,double x2,double y2) {
		return Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2));
	}
	
	public String toString() {
		return "Trie"+"["+bottomRightX+" "+bottomRightY+" "+topLeftX+" "+topLeftY+" "+capacity+"]";
	}
}
