package mpjp.quad;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import mpjp.shared.HasPoint;

class LeafTrie<T extends HasPoint> extends Trie<T> {
	
	// ---
	private Set<T> points;
	// ---

	public LeafTrie(double bottomRightX, double bottomRightY, double topLeftX, double topLeftY) {
		super(bottomRightX, bottomRightY, topLeftX, topLeftY);
		
		// ---
		this.points = new HashSet<T>();
		// ---
	}
	
	T find​(T point) {
		for (T p : points) {
			if (p.getX() == point.getX() && p.getY() == point.getY()) {
				return p;
			}
		}
		return null;
	}
	
	Trie<T> insert(T point) {
		points.add(point);
		LeafTrie<T> leaf = new LeafTrie<T>(bottomRightX, bottomRightY, topLeftX, topLeftY);
		leaf.points = points;
		return leaf;
	}
	
	Trie<T> insertReplace(T point) {
		points.remove(find​(point));
		return insert(point);
	}
	
	/**
	 * Collect points at a distance smaller or equal to radius from (x,y) and place them in given list
	 */	
	void collectNear(double x, double y, double radius, Set<T> nodes) {
		// (call overlap in line 50 ?)
		for (T p: points) {
			if (getDistance​(p.getX(),p.getY(),x,y) <= radius) {
				nodes.add(p);
			}
		}
	}
	
	void delete(T point) {
		if (points.contains(point)) {
			points.remove(point);
		}
	}
	
	void collectAll(Set<T> nodes) {
		nodes.addAll(points);
	}
	
	public String toString() {
		return "LeafTrie["+getPoints()+"]";
	}
	
	/**
	 * A collection of points currently in this leaf
	 * @returns collection of points
	 */
	Collection<T> getPoints() {
		return points;
	}

	public void accept(Visitor<T> visitor) {
		visitor.visit(this);
	}
}
