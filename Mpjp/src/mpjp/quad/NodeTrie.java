package mpjp.quad;

import mpjp.shared.HasPoint;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class NodeTrie<T extends HasPoint> extends Trie<T> {
	Map<Trie.Quadrant, Trie<T>> tries;
	
	/**
	 * Constructor
	 * @param bottomRightX
	 * @param bottomRightY
	 * @param topLeftX
	 * @param topLeftY
	 */
	public NodeTrie(double bottomRightX, double bottomRightY, double topLeftX, double topLeftY) {
		super(bottomRightX, bottomRightY, topLeftX, topLeftY);
		tries = new HashMap<>();
	}
	
	/*
	 * 		* (x1,y1) topLeft ----
	 * 		|	NW	  |		NE	  |
	 * 		|---------|-----------|
	 * 		|	SW	  |		SE	  |
	 * 		 -------------------- * (x2,y2) bottomRight
	 * 		^ y
	 * 		|
	 * 		---> x
	 * 
	 * 		NW -> (x1,y1) ; (x2/2,y1/2)
	 */
	/**
	 * Returns quadrant of given point
	 * @param point
	 * @return Quadrant bases on middle (bottomRight/2,topLeftY/2)
	 */
	Trie.Quadrant quadrantOf​(T point) {
		// TODO: quadrantOf --> Check if quadrants are correctly segmented
		double p_x = point.getX();
		double p_y = point.getY();
		
		double mid_x = bottomRightX/2;
		double mid_y = topLeftY/2;
		
		if (p_x >= mid_x && p_y >= mid_y) {
			return Quadrant.NE;
		} else if (p_x <= mid_x && p_y >= mid_y) {
			return Quadrant.NW;
		} else if (p_x <= mid_x && p_y <= mid_y) {
			return Quadrant.SW;
		} else if (p_x >= mid_x && p_y <= mid_y) {
			return Quadrant.SE;
		} else {
			throw new PointOutOfBoundException();
		}
	}
	
	T find​(T point) {
		Trie.Quadrant quadrant = quadrantOf​(point);
		Trie<T> res = tries.get(quadrant);
		
		if (res instanceof LeafTrie || res instanceof NodeTrie) {
			return res.find​(point);
		}
		
		return null;
	}
	
	/*
	 * 		* (tlX,tlY) --------- *			*1 - (brX/2, tlyY/2)
	 * 		|	 NW	   |	NE	  |						 ^ y	
	 * 		|--------- *1 --------|						 |
	 * 		|	 SW	   |	SE	  |					+ -> ---> x
	 * 		+ ------------------- * (brX,brY)
	 */
	Trie<T> insert(T point) {
		Trie.Quadrant quadrant = quadrantOf​(point);
		Trie<T> child = tries.get(quadrant);
		
		if (child == null) {
			switch (quadrant) {
				case NW:
					child = new LeafTrie<T>(bottomRightX/2, topLeftY/2, topLeftX, topLeftY);
					tries.put(Quadrant.NW, child);
					break;
				case NE:
					child = new LeafTrie<T>(bottomRightX, topLeftY/2, bottomRightX/2, topLeftY);
					tries.put(Quadrant.NE, child);
					break;
				case SW:
					child = new LeafTrie<T>(bottomRightX/2, bottomRightY, topLeftX, topLeftY/2);
					tries.put(Quadrant.SW, child);
					break;
				case SE:
					child = new LeafTrie<T>(bottomRightX, bottomRightY, bottomRightX/2, topLeftY/2);
					tries.put(Quadrant.SE, child);
					break;
				default:
					return null;
			}
		}
		if (child instanceof LeafTrie || child instanceof NodeTrie) {
			// System.out.println("Child not NULL");
			return child.insert(point);
		} else {
			System.out.println("Child is NULL");
			return null;
		}
	}
	
	Trie<T> insertReplace(T point) {
		Trie.Quadrant quadrant = quadrantOf​(point);
		Trie<T> child = tries.get(quadrant);
		
		if (child instanceof LeafTrie || child instanceof NodeTrie) {
			return child.insertReplace(point);
		}
		
		return null;
	}
	
	void delete(T point) {
		Trie.Quadrant quadrant = quadrantOf​(point);
		Trie<T> child = tries.get(quadrant);
		child.delete(point);
	}
	
	void collectNear(double x, double y, double radius, Set<T> nodes) { 
		for (Entry<Quadrant, Trie<T>> entry : tries.entrySet()) {
			Trie<T> child = entry.getValue();
			if (child instanceof NodeTrie || child instanceof LeafTrie) {
				child.collectNear(x, y, radius, nodes);
			}
		}
	}
		
	void collectAll(Set<T> nodes) {
		for (Entry<Quadrant, Trie<T>> entry : tries.entrySet()) { 
			entry.getValue().collectAll(nodes);
		}
	}
	
	public String toString() {
		return "NodeTrie [bottomRightX=" + bottomRightX + ", bottomRightY=" + bottomRightY + 
			   ", topLeftX=" + topLeftX+ ", topLeftY=" + topLeftY + "]";
	}
	
	public void accept(Visitor<T> visitor) {
		// TODO: accept
		visitor.visit(this);
	}
	
	/**
	 * A collection of tries that descend from this one
	 * @return
	 */
	Collection<Trie<T>> getTries() {
		return tries.values();
	}
}