package mpjp.game;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import mpjp.shared.MPJPException;
import mpjp.shared.PuzzleInfo;
import mpjp.shared.PuzzleLayout;
import mpjp.shared.PuzzleSelectInfo;
import mpjp.shared.PuzzleView;
import mpjp.shared.geom.Point;

public class Manager {
	
	private static Manager manager = null;
	static WorkspacePool pool = null;
	
	private Manager() {
		
	}
	
	/**
	 * The single instance of this class
	 * @return singleton
	 */
	public static Manager getInstance() {
		if(manager == null) {
			pool = new WorkspacePool();
			return manager = new Manager();
		} else
			return manager;
	}

	
	void reset() {
		//Manager.manager = getInstance();
		pool= new WorkspacePool();
	}
	
	/**
	 * A set of cutting names available to produce puzzle pieces
	 * @return set of cutting names
	 */
	public Set<String> getAvailableCuttings(){
		Set<String> cuttings = new HashSet<String>();
		cuttings.add("triangular");
		cuttings.add("standard");
		cuttings.add("round");
		cuttings.add("straight");
		return cuttings;
	}
	
	/**
	 * A set of images available for puzzle backgrounds
	 * @return set of images
	 */
	public Set<String> getAvailableImages(){
		Set<String> images = Images.getAvailableImages();
		return images;
	}
	
	/**
	 * A map of workspace IDs (int) to PuzzleSelectInfo. The IDs must be used to identify the intended workspace to use them with methods such as getPuzzleView(String), getCurrentLayout(String), selectPiece(String, Point), and connect(String, int, Point)
	 * @return map
	 */
	public Map<String,PuzzleSelectInfo> getAvailableWorkspaces(){
		return pool.getAvailableWorkspaces();
	}
	
	/**
	 * Create Workspace from info
	 * @param info
	 * @return workspace ID
	 * @throws MPJPException
	 */
	public String createWorkspace​(PuzzleInfo info) throws MPJPException{
		try {
			Workspace workspace = new Workspace(info);
			return workspace.getId();
		}catch(Exception e) {
			throw new MPJPException("Failed to create Workspace",e);
		}
	}
	
	/***
	 * Select a piece in the given workspace, with its "center" near the given point
	 * @param workspaceId
	 * @param point
	 * @return pieceID with null if no piece was selected, or the ID of the selected piece
	 * @throws MPJPException
	 */
	public Integer selectPiece​(String workspaceId, Point point) throws MPJPException{
		try {
			Workspace workspace = pool.getWorkspace​(workspaceId);
			return workspace.selectPiece​(point);
			
		}catch(Exception e) {
			throw new MPJPException("Failed to select piece",e);
		}
	}
	
	/**
	 * In the workspace with the given ID, connect piece with given iD after moving its center to the given point. All pieces in the same block are moved accordingly. Changed in workspace are persisted:
	 * @param workspaceId
	 * @param pieceId
	 * @param point
	 * @return updated layout
	 * @throws MPJPException
	 */
	public PuzzleLayout connect​(String workspaceId,int pieceId,Point point) throws MPJPException{
		try {
			Workspace workspace = pool.getWorkspace​(workspaceId);
			return workspace.connect​(pieceId, point);
			
		}catch(Exception e) {
			throw new MPJPException("",e);
		}
	}
	
	/**
	 * Puzzle view of given workspace
	 * @param workspaceId
	 * @return PuzzleView
	 * @throws MPJPException
	 */
	public PuzzleView getPuzzleView​(String workspaceId) throws MPJPException{
		try {
			Workspace workspace = pool.getWorkspace​(workspaceId);
			return workspace.getPuzzleView();
		}catch(Exception e) {
			throw new MPJPException("ID not valid",e);
		}
	}
	
	/**
	 * Current layout of the workspace with given ID
	 * @param workspaceId
	 * @return PuzzleLayout
	 * @throws MPJPException
	 */
	public PuzzleLayout getCurrentLayout​(String workspaceId) throws MPJPException{
		try {
			Workspace workspace = pool.getWorkspace​(workspaceId);
			return workspace.getCurrentLayout();
		}catch(Exception e) {
			throw new MPJPException("ID not valid",e);
		}
	}

}
