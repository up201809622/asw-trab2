package mpjp.game;

/*		      N (y--)
 *	W (x--)	(X,Y)	E (x++)
 *		      S (y++)
 */
public enum Direction {
	EAST {
		public int getSignalX() {return 1;}
		public int getSignalY() {return 0;}
	},
	NORTH {
		public int getSignalX() {return 0;}
		public int getSignalY() {return -1;}
	},
	SOUTH {
		public int getSignalX() {return 0;}
		public int getSignalY() {return 1;}
	},
	WEST {
		public int getSignalX() {return -1;}
		public int getSignalY() {return 0;}
	};
	
	public abstract int getSignalX();
	public abstract int getSignalY();
}
