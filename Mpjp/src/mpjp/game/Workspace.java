package mpjp.game;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import mpjp.game.cuttings.CuttingFactoryImplementation;
import mpjp.quad.PointQuadtree;
import mpjp.shared.MPJPException;
import mpjp.shared.PieceStatus;
import mpjp.shared.PuzzleInfo;
import mpjp.shared.PuzzleLayout;
import mpjp.shared.PuzzleSelectInfo;
import mpjp.shared.PuzzleView;
import mpjp.shared.geom.PieceShape;
import mpjp.shared.geom.Point;

public class Workspace implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// ---
	private PointQuadtree<PieceStatus> pieces;
	private Map<Integer, PieceStatus> layout_pieces;
	private Map<Integer, List<Integer>> layout_blocks;
	// private int puzzleLayoutPiecesCount;
	// private int puzzleLayoutBlocksCount;
	private int percentageSolved = 0;
	// ---
	
	PuzzleInfo info;
	Date start;
	
	static double rad;
	static double heightFactor;
	static double widthFactor;
	
	public Workspace(PuzzleInfo info) throws MPJPException {
		try {
			this.info=info;
			start= new Date();
		}catch(Exception e) {
			throw new MPJPException("Failed to create workspace",e);
		}
		scatter();
	}
	
	// Static ---

	public static double getWidthFactor() {
		return widthFactor;
	}


	public static void setWidthFactor(double widthFactor) {
		Workspace.widthFactor = widthFactor;
	}


	public static double getHeightFactor() {
		return heightFactor;
	}
	

	public static void setHeightFactor(double heightFactor) {
		Workspace.heightFactor = heightFactor;
	}

	public static double getRadius() {
		return rad;
	}
	

	public static void setRadius(double radius) {
		Workspace.rad = radius;
	}
	
	// ---
	
	String getId() {
		String id = info.getImageName() + start.getTime();
		return id;
	}
	
	/**
	 * Radius of height and width
	 * @return
	 */
	double getSelectRadius() {
		double maxHeight=info.getHeight();
		double maxWidth=info.getWidth();
		return Math.max(maxHeight, maxWidth)/2;
	}
	
	/**
	 * Information on this puzzle that may be used by a person to decide to help solving it. 
	 * Contains the data used to create the puzzle and the percentage that was already solved.
	 * @return PuzzleSelectInfo
	 */
	PuzzleSelectInfo getPuzzleSelectInfo() {
		PuzzleSelectInfo info = new PuzzleSelectInfo(this.info, start, this.getPercentageSolved());
		return info;
	}
	
	/**
	 * Percentage of puzzle solved
	 */
	int getPercentageSolved() {
		// PuzzleLayout layout = getCurrentLayout();
		//Map<Integer, PieceStatus> layout_pieces = this.layout_pieces;
		//Map<Integer, List<Integer>> layout_blocks = this.layout_blocks;
		
//		int b = layout_blocks.size(); // blocks.size();
//		int p = layout_pieces.size(); // pieces.size();
//		
//		if (p == b) {
//			return 0;
//		}
//		return 100*(p-b)/(p-1);
		
		getCurrentLayout();
		return percentageSolved;
	}
	
	/**
	 * Visual part of the puzzle, sent to the client when solving the puzzle.
	 * @return PuzzleView
	 * @throws MPJPException
	 */
	PuzzleView getPuzzleView() throws MPJPException {
		PuzzleStructure structure_info = getPuzzleStructure();
		double width = Workspace.getWidthFactor()*info.getWidth();
		double height = Workspace.getHeightFactor()*info.getHeight();

		Map<Integer,PieceShape> shapes = new CuttingFactoryImplementation().createCutting("triangular").getShapes(structure_info);
		shapes.putAll(new CuttingFactoryImplementation().createCutting("round").getShapes(structure_info));
		shapes.putAll(new CuttingFactoryImplementation().createCutting("straight").getShapes(structure_info));
		shapes.putAll(new CuttingFactoryImplementation().createCutting("standard").getShapes(structure_info));
		Map<Integer,Point> locations = getPuzzleStructure().getStandardLocations();
		
		PuzzleView view = new PuzzleView(this.start, width , height ,structure_info.getWidth(), structure_info.getHeight(), 
				structure_info.getPieceWidth(), structure_info.getPieceHeight(), info.getImageName(),shapes, locations);
		return view;
	}
	
	/**
	 * Current piece positions and blocks formed by connected pieces
	 * @return PuzzleLayout
	 */
	PuzzleLayout getCurrentLayout() {
		// TODO: getCurrentLayout
		Map<Integer, PieceStatus> layout_pieces = new HashMap<Integer, PieceStatus>();
		Map<Integer, List<Integer>> layout_blocks = new HashMap<Integer, List<Integer>>();
		
		Set<PieceStatus> quad_pieces = pieces.getAll();
		
		for (PieceStatus ps : quad_pieces) {
			List<Integer> ids_in_block = new Vector<>();
			int piece_id = ps.getId();
			int block_id = ps.getBlock();
			
			layout_pieces.put(piece_id, ps);
			
			if (layout_blocks.get(block_id) == null) {
				ids_in_block.add(piece_id);
				layout_blocks.put(block_id, ids_in_block);
			} else {
				ids_in_block = layout_blocks.get(block_id);
				ids_in_block.add(piece_id);
				if (piece_id < block_id) {
					block_id = piece_id;
				}
				layout_blocks.put(block_id, ids_in_block);
			}
		}
		
		this.layout_pieces = layout_pieces;
		this.layout_blocks = layout_blocks;
		
		int p = layout_pieces.size();
		int b = layout_blocks.size();
		percentageSolved = 100*(p-b)/(p-1);
		
		// System.out.println("Piece Count: "+puzzleLayoutPiecesCount);
		// System.out.println("Block Count: "+puzzleLayoutBlocksCount);
		
		return new PuzzleLayout(layout_pieces, layout_blocks, percentageSolved);
	}
	
	/**
	 * Scatter the puzzle's pieces on the board before solving it. This method is invoked when this workspace is instantiated.
	 */
	void scatter() {
		PuzzleStructure puzzleStructure = getPuzzleStructure();
		double width = info.getWidth();
		double height = info.getHeight();
		pieces = new PointQuadtree<PieceStatus>(width, height);
		
		int id = 0;
		for(int row=0; row < info.getRows(); row++) {
			for(int column=0; column < info.getColumns(); column++) {
				double x = ( column + 0.5D ) * puzzleStructure.getPieceWidth();
				double y = ( row + 0.5D ) * puzzleStructure.getPieceHeight();
				
				PieceStatus ps = new PieceStatus(id, new Point(x,y));
				ps.setBlock(id);
				pieces.insert(ps);
				id++;
			}
		}
	}
	
	/**
	 * Restore transient fields that are not saved by serialization. In particular, the PointQuadtree is not serializable and can be easily restored from pieces.
	 */
	void restore() {
		scatter();
		getCurrentLayout();
	}
	
	void restoreInfo(PuzzleInfo info) {
		this.info = info;
	}
	
	void setQuadToNull() {
		pieces = null;
	}
	
	/**
	 * Select Piece bases on point
	 * @param point
	 * @return
	 */
	Integer selectPiece​(Point point){
		//Map<Integer, PieceStatus> map = getCurrentLayout().getPieces();
		
		System.out.println("layout pieces size: "+layout_pieces.size());
		
		for(Map.Entry<Integer, PieceStatus> entry : layout_pieces.entrySet()) {
			if((entry.getValue().getPosition().getX()==point.getX()) && (entry.getValue().getPosition().getY()==point.getY())) {
				return entry.getKey();
			}	
		}
		
		Set<PieceStatus> set = pieces.findNear(point.getX(), point.getY(), getRadius());
		System.out.println("set size: "+set.size());
		for (PieceStatus ps : set) {
			return ps.getId();
		}
		
			return null;
	}
	
	/**
	 * Check if given id exists
	 * @param id
	 * @return bool
	 */
	boolean checkID(int id) {
		Map<Integer, PieceStatus> map = getCurrentLayout().getPieces();
		for(Map.Entry<Integer, PieceStatus> entry : map.entrySet()) {
			if (entry.getKey() == id) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Move the piece with given id to given point and check if it connects with other pieces. Pieces in the same block are moved accordingly. If two (or more pieces are connected) then they will merged into a single block. The new merged block will have as ID of the lowest ID.
	 * If one of more pieces fall off the workspace the a exception is raised. However, all affected pieces are rolled back to their previous position.
	 * @param id
	 * @param point
	 * @return PuzzleLayout
	 * @throws MPJPException
	 */
	PuzzleLayout connect​(int id,Point point) throws MPJPException {
		PuzzleView view = getPuzzleView();
		double width = view.getWorkspaceWidth();
		double height = view.getWorkspaceHeight();
		
		if (id < 0 || !checkID(id)) {
			throw new MPJPException("ID not valid");
		} else if (point.getX() >= width || point.getY() >= height || point.getX() < 0 || point.getY() < 0) {
			throw new MPJPException("Moved piece falls out of puzzle");
		} 
	
		Map<Integer, PieceStatus> id_ps = getCurrentLayout().getPieces();
		Map<Integer, List<Integer>> idb_idps = getCurrentLayout().getBlocks();
		PieceStatus ps = id_ps.get(id); // PieceStatus never null
		
		pieces.delete(ps);
		ps.setPosition(point);
		id_ps.put(id, ps);
		pieces.insert(ps);
		
		Set<PieceStatus> pieces_near = pieces.findNear(point.getX(), point.getY(), getRadius());
		if (pieces_near.size() != 0) {	// Check if pieces connect , they connect if near
			List<Integer> list = new Vector<>();
			list.add(id);
			int sml_id = id;
			for (PieceStatus ps_near : pieces_near) {
				int ps_id = ps_near.getId();
				int b_id  = ps_near.getBlock();
				
				// All pieces , including ps, must be considered as a block
				if (ps_id < sml_id) {
					sml_id = ps_id;
				}
				idb_idps.remove(b_id);
				list.add(ps_id);
			}
			idb_idps.put(sml_id, list);
		}
		
		this.layout_pieces = id_ps;
		this.layout_blocks = idb_idps;
		
		// puzzleLayoutPiecesCount = id_ps.size();
		// puzzleLayoutBlocksCount = idb_idps.size();
		
		return new PuzzleLayout(id_ps, idb_idps, getPercentageSolved());
	}
	
	/**
	 * Puzzle structure used in this workspace's puzzle. This data is needed for unit testing.
	 * @return puzzle structure
	 */
	PuzzleStructure getPuzzleStructure() {
		PuzzleStructure structure = new PuzzleStructure(this.info);
		setWidthFactor(structure.getWidth()/structure.getPieceWidth());
		setHeightFactor(structure.getHeight()/structure.getPieceHeight());
		return structure;
	}
}
