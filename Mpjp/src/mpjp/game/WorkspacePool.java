package mpjp.game;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import mpjp.shared.MPJPException;
import mpjp.shared.PieceStatus;
import mpjp.shared.PuzzleInfo;
import mpjp.shared.PuzzleSelectInfo;

public class WorkspacePool {
	static final String SERIALIAZTION_SUFFIX = ".ser";
	static String WORKING_DIR = ".";
	static Map<String,PuzzleSelectInfo> ids;
	private Map<String,File> files;
	
	public WorkspacePool() {
		files = new HashMap<String, File>();
		ids = new HashMap<String, PuzzleSelectInfo>();
	}

	/**
	 * Current pool directory, the directory where workspace serializations are saved.
	 * @return
	 */
	public static File getPoolDirectory() {
		return new File(WORKING_DIR);
	}
	
	/**
	 * Change pool directory, the directory where workspace serializations are saved.
	 * @param poolDirectory
	 */
	public static void setPoolDiretory​(File poolDirectory) {
		WORKING_DIR = poolDirectory.getPath();
	}
	
	/**
	 * Convenience method for setting pool directory as a string
	 * @param pathname
	 */
	public static void setPoolDirectory​(String pathname) {
		WORKING_DIR = pathname;
	}

	/**
	 * Create a workspace that is stored using its id as key. The new workspace is also serialized.
	 * @param info
	 * @return
	 * @throws MPJPException
	 */
	String createWorkspace​(PuzzleInfo info) throws MPJPException {
		try {
			Workspace ws = new Workspace(info);
			ids.put(ws.getId(), new PuzzleSelectInfo(info, new Date(), 0));
			this.backup​(ws.getId(), ws);
			return ws.getId();
		}
		catch (Exception e) {
			throw new MPJPException("Error creating workspace", e);
		}
	}
	
	/**
	 * Get the workspace with given id. If it is not available in memory then it will be recovered from a serialized file
	 * @param id
	 * @return
	 * @throws MPJPException
	 */
	Workspace getWorkspace​(String id) throws MPJPException {
		// If it is not available in memory then it will be recovered from a serialized file.
		boolean flag = false;
		Workspace ws = null;
		
		for (Map.Entry<String,PuzzleSelectInfo> entry : ids.entrySet()) { 
			if (entry.getKey().equals(id)) {
				flag = true;
				ws = new Workspace(entry.getValue());
			}
		}
		if (!flag) {
			try {
				String filename = WORKING_DIR+id+SERIALIAZTION_SUFFIX;
				FileInputStream fileIn = new FileInputStream(filename);
				ObjectInputStream objIn = new ObjectInputStream(fileIn);
				
				ws = (Workspace) objIn.readObject();
				ws.restoreInfo(ws.info);
				ws.restore();
				fileIn.close();
				objIn.close();
			} catch(Exception e) {
				throw new MPJPException("Failed to get workspace", e);
			}
		}
		return ws;
	}
	
	/**
	 * A map of workspace IDs to PuzzleSelectInfo used for selecting an existing puzzle to solve.
	 * @return
	 */
	Map<String,PuzzleSelectInfo> getAvailableWorkspaces() {
		return ids;
	}
	
	/**
	 * A File object for given ID. The file name is the ID with suffix .ser and its parent directory is given by property poolDirectory. File objects are stored to avoid being constantly recreated.
	 * @param workspaceId
	 * @return
	 */
	File getFile​(String workspaceId) {
		for (Entry<String, File> entry : files.entrySet()) { 
			if (entry.getKey().equals(workspaceId)) {
				return entry.getValue();
			}
		}
		File temp = new File(WORKING_DIR, workspaceId+SERIALIAZTION_SUFFIX);
		files.put(workspaceId, temp);
		return temp;
	}
	
	/**
	 * Serializes workspace
	 * @param workspaceId
	 * @param workspace
	 * @throws MPJPException
	 */
	void backup​(String workspaceId, Workspace workspace) throws MPJPException {
		String filename = WORKING_DIR+workspaceId+SERIALIAZTION_SUFFIX;
		File file = new File(WORKING_DIR, workspaceId+SERIALIAZTION_SUFFIX);
		try {
			file.createNewFile();
			files.put(workspaceId, file); // Add file to files (getFile())
			FileOutputStream fileOut = new FileOutputStream(filename);
			ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
			
			workspace.setQuadToNull();
			objOut.writeObject(workspace); // Backup complete
			objOut.close();
			fileOut.close();
		} catch(Exception e) {
			throw new MPJPException("Failed to backup workspace", e);
		}
	}
	
	/**
	 * Recover a workspace form a serialization given its ID
	 * @param workspaceId
	 * @return
	 * @throws MPJPException
	 */
	Workspace recover​(String workspaceId) throws MPJPException {
		try {
			String filename = WORKING_DIR+workspaceId+SERIALIAZTION_SUFFIX;
			FileInputStream fileIn = new FileInputStream(filename);
			ObjectInputStream objIn = new ObjectInputStream(fileIn);
			
			Workspace ws = (Workspace) objIn.readObject();
			ws.restoreInfo(ws.info);
			ws.restore();
			fileIn.close();
			objIn.close();
			return ws;					  // Recover complete
		} catch(Exception e) {
			throw new MPJPException("Failed to recover workspace", e);
		}
	}
	
	/**
	 * Revert all fields to their original values. Reset the pool for testing purposes. Do not use this method in production!
	 */
	void reset() {
		WORKING_DIR = ".";
		ids = new HashMap<String,PuzzleSelectInfo>();
		files = new HashMap<String,File>();
	}
}
