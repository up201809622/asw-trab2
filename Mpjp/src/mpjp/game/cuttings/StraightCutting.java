package mpjp.game.cuttings;
import mpjp.game.PuzzleStructure;
import mpjp.shared.geom.LineTo;
import mpjp.shared.geom.PieceShape;
import mpjp.shared.geom.Point;

import java.util.HashMap;
import java.util.Map;


public class StraightCutting implements Cutting {

	public StraightCutting() {}
	
	public Map<Integer,PieceShape> getShapes(PuzzleStructure structure) {
		Map<Integer,PieceShape> shapes_map = new HashMap<Integer,PieceShape>();
		
		double piece_width = structure.getPieceWidth();
		double piece_height = structure.getPieceHeight();
		int rows = structure.getRows();
		int cols = structure.getColumns();
		
		int piece_index = 0;
		
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Point str_p = new Point(piece_width/2,piece_height/2);
				
				PieceShape ps = new PieceShape(str_p)
					.addSegment​( new LineTo( new Point( str_p.getX(),  -str_p.getY() )))
					.addSegment​( new LineTo( new Point( -str_p.getX(), -str_p.getY() )))
					.addSegment​( new LineTo( new Point( -str_p.getX(), str_p.getY() )))
					.addSegment​( new LineTo( str_p ));
				
				shapes_map.put(piece_index, ps);
				piece_index++;
			}
		}
		return shapes_map;
	}

}
