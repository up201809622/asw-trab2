package mpjp.game.cuttings;

import java.util.HashMap;
import java.util.Map;

import mpjp.game.Direction;
import mpjp.game.PuzzleStructure;
import mpjp.shared.MPJPException;
import mpjp.shared.geom.CurveTo;
import mpjp.shared.geom.LineTo;
import mpjp.shared.geom.PieceShape;
import mpjp.shared.geom.Point;

public class StandardCutting extends Object implements Cutting{
	
	PuzzleStructure structure;
	
	public StandardCutting() {
		
	}
	
	public Map<java.lang.Integer,PieceShape> getShapes(PuzzleStructure structure){
		this.structure=structure;
		Map<Integer,PieceShape> shapes = new HashMap<Integer,PieceShape>();
		double pieceWidth = structure.getPieceWidth();
		double pieceHeight = structure.getPieceHeight();
		int numberRows = structure.getRows();
		int numberColumns = structure.getColumns();
		int pieceNumber=0;

		for(int i=0;i<numberRows;i++) {
			for(int j=0;j<numberColumns;j++) {
				Point startingPoint = new Point(pieceWidth/2,pieceHeight/2); 
				PieceShape pieceShape = new PieceShape(startingPoint);
				if(i==0) { //Primeira linha
					if(j==0) {
						pieceShape.addSegment​(new LineTo(getStartControlPoint1​(numberColumns*i+j,Direction.NORTH)));
						pieceShape.addSegment​(new CurveTo(getStartControlPoint1​(numberColumns*i+j,Direction.NORTH),getMiddlePoint​(numberColumns*i+j,Direction.NORTH),getEndControlPoint1​(numberColumns*i+j,Direction.NORTH)));
						pieceShape.addSegment​(new LineTo(new Point(startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new LineTo(new Point(-startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new LineTo(new Point(-startingPoint.getX(),startingPoint.getY())));
						pieceShape.addSegment​(new LineTo(getStartControlPoint2​(numberColumns*i+j,Direction.WEST)));
						pieceShape.addSegment​(new CurveTo(getStartControlPoint2​(numberColumns*i+j,Direction.WEST),getMiddlePoint​(numberColumns*i+j,Direction.WEST),getEndControlPoint2(numberColumns*i+j,Direction.WEST)));
						pieceShape.addSegment​(new LineTo(new Point(startingPoint.getX(),startingPoint.getY())));
					}
					else if(j==numberColumns-1) {
						
					}
					else {
						pieceShape.addSegment​(new LineTo(getStartControlPoint1​(numberColumns*i+j,Direction.NORTH)));
						pieceShape.addSegment​(new CurveTo(getStartControlPoint1​(numberColumns*i+j,Direction.NORTH),getMiddlePoint​(numberColumns*i+j,Direction.NORTH),getEndControlPoint1​(numberColumns*i+j,Direction.NORTH)));
					}
				}

				else if (i==numberRows-1) { //última linha
					if(j==0) {
						
					}
					else if(j==numberColumns-1) {
					}

					else {
						
					}	
				}
				else { //linhas do meio
					if(j==0) {
						
					}
					else if(j==numberColumns-1) {
						
					}

					else {
						
					}	
				}
			
				shapes.put(pieceNumber, pieceShape);
				pieceNumber++;
			}
		}
		return shapes;
	}
	
	Point getMiddlePoint​(int id, Direction direction) {
		try {
			Point point = this.structure.getPieceStandardCenter​(id);
			switch(direction) {
			case NORTH:
				return new Point(point.getX()/2,point.getY()-structure.getPieceHeight()/2);
			case WEST:
				return new Point(point.getX()-structure.getPieceWidth()/2,point.getY()/2);
			default: return null;
			}
		} catch (MPJPException e) {
			return null;
		}
	}
	
	Point getStartControlPoint1​(int id, Direction direction) {
		try {
			Point point = this.structure.getPieceStandardCenter​(id);
			switch(direction) {
			case NORTH:
				return new Point(point.getX(),point.getY()-structure.getPieceHeight()/3);
			default: return null;
			}
		} catch (MPJPException e) {
			return null;
		}
	}
	
	Point getStartControlPoint2​(int id, Direction direction) {
		try {
			Point point = this.structure.getPieceStandardCenter​(id);
			switch(direction) {
			case WEST:
				return new Point(-2*(structure.getPieceWidth()/3)+point.getX(),point.getY());
			default: return null;
			}
		} catch (MPJPException e) {
			return null;
		}
	}
	
	Point getEndControlPoint1​(int id, Direction direction) {
		try {
			Point point = this.structure.getPieceStandardCenter​(id);
			switch(direction) {
			case NORTH:
				return new Point(point.getX(),point.getY()-2*(structure.getPieceHeight()/3));
			default: return null;
			}
		} catch (MPJPException e) {
			return null;
		}
	}
	
	Point getEndControlPoint2(int id, Direction direction) {
		try {
			Point point = this.structure.getPieceStandardCenter​(id);
			switch(direction) {
			case WEST:
				return new Point(point.getX()-structure.getPieceWidth()/3,point.getY());
			default: return null;
			}
		} catch (MPJPException e) {
			return null;
		}
	}
}
