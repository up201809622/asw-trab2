package mpjp.game.cuttings;

import java.util.HashMap;
import java.util.Map;

import mpjp.game.PuzzleStructure;
import mpjp.shared.geom.LineTo;
import mpjp.shared.geom.PieceShape;
import mpjp.shared.geom.Point;

public class TriangularCutting implements Cutting {

	public TriangularCutting() {}

	@Override
	public Map<Integer, PieceShape> getShapes(PuzzleStructure structure) {
		Map<Integer, PieceShape> shapes = new HashMap<Integer, PieceShape>();
		
		double piece_width = structure.getPieceWidth();
		double piece_height = structure.getPieceHeight();
		int rows = structure.getRows();
		int cols = structure.getColumns();
		
		int piece_index = 0;
		
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Point str_p = new Point(piece_width/2,piece_height/2);
				PieceShape ps = new PieceShape(str_p);
				
				if (i == 0) { // fst line
					if (j == 0) {
						ps.addSegment​(new LineTo(new Point(0,str_p.getY()/2)));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),str_p.getY())));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),0))); 
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),-str_p.getY())));
						ps.addSegment​(new LineTo(new Point(0,-str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(str_p.getX(),-str_p.getY())));
						ps.addSegment​(new LineTo(new Point(str_p.getX()/2,0)));
						ps.addSegment​(new LineTo(str_p));
					} else if(j==cols-1) {
						ps.addSegment​(new LineTo(new Point(0,str_p.getY()/2)));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),str_p.getY())));
						ps.addSegment​(new LineTo(new Point(-str_p.getX()-str_p.getX()/2,0)));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),-str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(0,-str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(str_p.getX(),-str_p.getY())));
						ps.addSegment​(new LineTo(new Point(str_p.getX(),0)));
						ps.addSegment​(new LineTo(str_p));
					} else {
						ps.addSegment​(new LineTo(new Point(0,str_p.getY()/2)));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),str_p.getY())));
						ps.addSegment​(new LineTo(new Point(-str_p.getX()-str_p.getX()/2,0)));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),-str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(0,-str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(str_p.getX(),-str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(str_p.getX()/2,0)));
						ps.addSegment​(new LineTo(str_p));
					}
				} else if (i == rows-1) { // lst line
					if(j==0) {
						ps.addSegment​(new LineTo(new Point(0,str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),str_p.getY())));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),0))); 
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),-str_p.getY())));
						ps.addSegment​(new LineTo(new Point(0,-str_p.getY()-str_p.getY()/2)));
						ps.addSegment​(new LineTo(new Point(str_p.getX(),-str_p.getY())));
						ps.addSegment​(new LineTo(new Point(str_p.getX()/2,0)));
						ps.addSegment​(new LineTo(str_p));
					} else if(j==cols-1) {
						ps.addSegment​(new LineTo(new Point(0,str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),str_p.getY())));
						ps.addSegment​(new LineTo(new Point(-str_p.getX()-str_p.getX()/2,0)));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),-str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(0,-str_p.getY()-str_p.getY()/2)));
						ps.addSegment​(new LineTo(new Point(str_p.getX(),-str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(str_p.getX(),0)));
						ps.addSegment​(new LineTo(str_p));
					} else {
						ps.addSegment​(new LineTo(new Point(0,str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),str_p.getY())));
						ps.addSegment​(new LineTo(new Point(-str_p.getX()-str_p.getX()/2,0)));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),-str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(0,-str_p.getY()-str_p.getY()/2)));
						ps.addSegment​(new LineTo(new Point(str_p.getX(),-str_p.getY())));
						ps.addSegment​(new LineTo(new Point(str_p.getX()/2,0)));
						ps.addSegment​(new LineTo(str_p));
					}	
				} else { // mid lines
					if(j==0) {
						ps.addSegment​(new LineTo(new Point(0,str_p.getY()/2)));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),str_p.getY())));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),0))); 
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),-str_p.getY())));
						ps.addSegment​(new LineTo(new Point(0,-str_p.getY()-str_p.getY()/2)));
						ps.addSegment​(new LineTo(new Point(str_p.getX(),-str_p.getY())));
						ps.addSegment​(new LineTo(new Point(str_p.getX()/2,0)));
						ps.addSegment​(new LineTo(str_p));
					}
					else if(j==cols-1) {
						ps.addSegment​(new LineTo(new Point(0,str_p.getY()/2)));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),str_p.getY())));
						ps.addSegment​(new LineTo(new Point(-str_p.getX()-str_p.getX()/2,0)));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),-str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(0,-str_p.getY()-str_p.getY()/2)));
						ps.addSegment​(new LineTo(new Point(str_p.getX(),-str_p.getY())));
						ps.addSegment​(new LineTo(new Point(str_p.getX(),0)));
						ps.addSegment​(new LineTo(str_p)); 
					}

					else {
						ps.addSegment​(new LineTo(new Point(0,str_p.getY()/2)));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(-str_p.getX()-str_p.getX()/2,0)));
						ps.addSegment​(new LineTo(new Point(-str_p.getX(),-str_p.getY()))); 
						ps.addSegment​(new LineTo(new Point(0,-str_p.getY()-str_p.getY()/2)));
						ps.addSegment​(new LineTo(new Point(str_p.getX(),-str_p.getY())));
						ps.addSegment​(new LineTo(new Point(str_p.getX()/2,0)));
						ps.addSegment​(new LineTo(str_p));
					}
				}
				shapes.put(piece_index, ps);
				piece_index++;
			}
		}
		
		return shapes;
	}

}
