package mpjp.game.cuttings;

import java.util.HashSet;
import java.util.Set;

import mpjp.shared.MPJPException;

public class CuttingFactoryImplementation extends Object implements CuttingFactory{
	
	public CuttingFactoryImplementation() {};
		
	public Set<String> getAvaliableCuttings(){
		Set<java.lang.String> cuttings = new HashSet<String>();
		cuttings.add("triangular");
		cuttings.add("standard");
		cuttings.add("round");
		cuttings.add("straight");
		return cuttings;
	}



	@Override
	public Cutting createCutting(String name) throws MPJPException {
		switch(name) {
			case "triangular":
				return new TriangularCutting();
			case "round":
				return new RoundCutting();
			case "straight":
				return new StraightCutting();
			case "standard":
				return new StandardCutting();
			default:
				throw new MPJPException("Cutting not created");
		}
	}
}
