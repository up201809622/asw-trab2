package mpjp.game.cuttings;

import java.util.Set;

import mpjp.shared.MPJPException;

public interface CuttingFactory {
	/***
	 * Create a cutting with given name. Provided name must be one of given by getAvaliableCuttings()
	 * @param name
	 * @return cutting
	 * @throws MPJPException
	 */
	Cutting createCutting(String name) throws MPJPException;
	
	/**
	 * Set of valid cutting names. They can be used as argument for createCutting(String)
	 * @return set of valid cutting names
	 */
	Set<java.lang.String> getAvaliableCuttings();
}
