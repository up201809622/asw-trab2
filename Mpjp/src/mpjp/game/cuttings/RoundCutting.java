package mpjp.game.cuttings;

import java.util.HashMap;
import java.util.Map;

import mpjp.game.PuzzleStructure;
import mpjp.shared.geom.PieceShape;
import mpjp.shared.geom.Point;
import mpjp.shared.geom.QuadTo;

public class RoundCutting extends Object implements Cutting {

	public RoundCutting() {
		
	}
	
	
	
	public Map<Integer,PieceShape> getShapes(PuzzleStructure structure){
		Map<Integer,PieceShape> shapes = new HashMap<Integer,PieceShape>();
		double pieceWidth = structure.getPieceWidth();
		double pieceHeight = structure.getPieceHeight();
		int numberRows = structure.getRows();
		int numberColumns = structure.getColumns();
		int pieceNumber=0;

		for(int i=0;i<numberRows;i++) {
			for(int j=0;j<numberColumns;j++) {
				Point startingPoint = new Point(pieceWidth/2,pieceHeight/2); 
				PieceShape pieceShape = new PieceShape(startingPoint);

				if(i==0) {
					if(j==0) {
						pieceShape.addSegment​(new QuadTo(new Point(0,startingPoint.getY()/2),new Point(-startingPoint.getX(),startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(-startingPoint.getX(),-startingPoint.getY()),new Point(-startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(startingPoint.getX(),-startingPoint.getY()),new Point(startingPoint.getX(),-startingPoint.getY()))); 
						pieceShape.addSegment​(new QuadTo(new Point(startingPoint.getX()/2,0),new Point(startingPoint.getX(),startingPoint.getY())));
					}
					else if(j==numberColumns-1) {
						pieceShape.addSegment​(new QuadTo(new Point(0,startingPoint.getY()/2),new Point(-startingPoint.getX(),startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(-startingPoint.getX()-startingPoint.getX()/2,0),new Point(-startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(startingPoint.getX(),-startingPoint.getY()),new Point(startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(startingPoint.getX(),startingPoint.getY()),new Point(startingPoint.getX(),startingPoint.getY()))); 
					}
					else {
						pieceShape.addSegment​(new QuadTo(new Point(0,startingPoint.getY()/2),new Point(-startingPoint.getX(),startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(-startingPoint.getX()-startingPoint.getX()/2,0),new Point(-startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(startingPoint.getX(),-startingPoint.getY()),new Point(startingPoint.getX(),-startingPoint.getY()))); 
						pieceShape.addSegment​(new QuadTo(new Point(startingPoint.getX()/2,0),new Point(startingPoint.getX(),startingPoint.getY())));
					}
				}

				else if (i==numberRows-1) { 
					if(j==0) {
						pieceShape.addSegment​(new QuadTo(new Point(-startingPoint.getX(),startingPoint.getY()),new Point(-startingPoint.getX(),startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(-startingPoint.getX(),-startingPoint.getY()),new Point(-startingPoint.getX(),-startingPoint.getY()))); 
						pieceShape.addSegment​(new QuadTo(new Point(0,-startingPoint.getY()-startingPoint.getY()/2),new Point(startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(startingPoint.getX()/2,0),new Point(startingPoint.getX(),startingPoint.getY())));
					}
					else if(j==numberColumns-1) {
						pieceShape.addSegment​(new QuadTo(new Point(-startingPoint.getX(),startingPoint.getY()),new Point(-startingPoint.getX(),startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(-startingPoint.getX()-startingPoint.getX()/2,0),new Point(-startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(0,-startingPoint.getY()-startingPoint.getY()/2),new Point(startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(startingPoint.getX(),startingPoint.getY()),new Point(startingPoint.getX(),startingPoint.getY())));
					}

					else {
						pieceShape.addSegment​(new QuadTo(new Point(-startingPoint.getX(),startingPoint.getY()),new Point(-startingPoint.getX(),startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(-startingPoint.getX()-startingPoint.getX()/2,0),new Point(-startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(0,-startingPoint.getY()-startingPoint.getY()/2),new Point(startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(startingPoint.getX()/2,0),new Point(startingPoint.getX(),startingPoint.getY())));
					}	
				}
				else {
					if(j==0) {
						pieceShape.addSegment​(new QuadTo(new Point(0,startingPoint.getY()/2),new Point(-startingPoint.getX(),startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(-startingPoint.getX(),-startingPoint.getY()),new Point(-startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(0,-startingPoint.getY()-startingPoint.getY()/2),new Point(startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(startingPoint.getX()/2,0),new Point(startingPoint.getX(),startingPoint.getY())));
					
					}
					else if(j==numberColumns-1) {
						pieceShape.addSegment​(new QuadTo(new Point(0,startingPoint.getY()/2),new Point(-startingPoint.getX(),startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(-startingPoint.getX()-startingPoint.getX()/2,0),new Point(-startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(0,-startingPoint.getY()-startingPoint.getY()/2),new Point(startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(startingPoint.getX(),startingPoint.getY()),new Point(startingPoint.getX(),startingPoint.getY()))); 
					}

					else {
						pieceShape.addSegment​(new QuadTo(new Point(0,startingPoint.getY()/2),new Point(-startingPoint.getX(),startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(-startingPoint.getX()-startingPoint.getX()/2,0),new Point(-startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(0,-startingPoint.getY()-startingPoint.getY()/2),new Point(startingPoint.getX(),-startingPoint.getY())));
						pieceShape.addSegment​(new QuadTo(new Point(startingPoint.getX()/2,0),new Point(startingPoint.getX(),startingPoint.getY())));
					}	
				}
			
				shapes.put(pieceNumber, pieceShape);
				pieceNumber++;
			}
		}
		return shapes;
	}
		
}
