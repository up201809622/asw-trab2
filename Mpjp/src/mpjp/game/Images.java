package mpjp.game;
import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Images extends Object {
	
	static Set<String> extensions=new HashSet<String>(Arrays.asList("jpg"));
	static File	imageDirectory = new File("test/mpjp/resources");
	
	/**
	 * Get image directory
	 * @return imageDirectory
	 */
	public static File getImageDirectory() {
		return imageDirectory;
	}

	/**
	 * Set image directory
	 * @param imageDirectory
	 */
	public static void setImageDirectory(File imageDirectory) {
		Images.imageDirectory = imageDirectory;
	}

	/**
	 * Get set of valid extensions
	 * @return extensions
	 */
	public static Set<String> getExtensions(){
		return extensions;
		
	}
	
	/**
	 * Add extension to set of extensions
	 * @param extension
	 */
	public static void addExtension(String extension) {
		extensions.add(extension);
	}
	
	/**
	 * Get set of image names that can be used in puzzles
	 */
	static Set<String> getAvailableImages(){
		Set<String> images = new HashSet<String>();
		String[] array_images = imageDirectory.list();
		
		for(String i: array_images) {
			images.add(i);
		}
		
		return images;
		
	}
}
