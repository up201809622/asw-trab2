package mpjp.game;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import mpjp.shared.MPJPException;
import mpjp.shared.PuzzleInfo;
import mpjp.shared.geom.Point;

public class PuzzleStructure implements java.lang.Iterable<java.lang.Integer> {
	int rows;
	int columns;
	double width;
	double height;
	
	/**
	 * Create an instance from raw data
	 * @param rows
	 * @param columns
	 * @param width
	 * @param height
	 */
	public PuzzleStructure(int rows, int columns, double width, double height) {
		this.rows = rows;
		this.columns = columns;
		this.width = width;
		this.height = height;
	}

	/**
	 * Create instance from data in PuzzleInfo
	 * @param info
	 */
	public PuzzleStructure(PuzzleInfo info) {
		this.rows = info.getRows();
		this.columns = info.getColumns();
		this.width = info.getWidth();
		this.height = info.getHeight();
	}
	
	void init(int rows, int columns, double width, double height) {
		this.rows = rows;
		this.columns = columns;
		this.width = width;
		this.height = height;
	}
	
	// -- Getters --

	public int getRows() {
		return rows;
	}
	
	public int getPieceRow(int id) throws MPJPException {
		if (id < 0 || id >= getPieceCount()) { 
			throw new MPJPException("ID not found");
		}
		return id/columns;
	}
	
	public int getColumns() {
		return columns;
	}
	
	public int getPieceColumn(int id) throws MPJPException {
		if (id < 0 || id >= getPieceCount()) {
			throw new MPJPException("ID not found");
		}
		return id%columns;
	}

	public double getWidth() {
		return width;
	}
	
	public double getPieceWidth() {
		return width/columns;
	}
	
	public double getHeight() {
		return height;
	}
	
	public double getPieceHeight() {
		return height/rows;
	}
	
	public int getPieceCount() {
		return rows*columns;
	}
	
	/**
	 * Get piece facing in given direction based on given id
	 * @param direction
	 * @param id
	 * @return id of facing piece, or null if no piece is facing in that direction
	 */
	public Integer getPieceFacing(Direction direction, Integer id) {
		try {
			int y = getPieceRow(id);
			int x = getPieceColumn(id);
			
			switch (direction) {
			case NORTH:
				y--;
				break;
			case SOUTH:
				y++;
				break;
			case WEST:
				x--;
				break;
			case EAST:
				x++;
				break;
			default:
				return null;
			}
			if (y < 0 || x < 0 || x >= columns || y >= rows) { return null; }
			
			return (columns*y)+x;
		}
		
		catch (Exception e) { return null; }
	}
	
	/**
	 * Get piece center facing direction in point
	 * @param direction
	 * @param point
	 * @return center of matching piece
	 */
	Point getPieceCenterFacing​(Direction direction, Point point) {
		double x = point.getX();
		double y = point.getY();
		
		final double height = getPieceHeight();
		final double width  = getPieceWidth();
		
		switch (direction) {
			case NORTH:
				return new Point(x,y-height);
			case SOUTH:
				return new Point(x,y+height);
			case WEST:
				return new Point(x-width,y);
			case EAST:
				return new Point(x+width,y);
			default:
				return null;
		}
	}
	
	// -- Standards --
	// piece_width(height)/2  --> Offset to center | getPieceRow(id)*piece_width --> distance between piece and (0,0)
	/**
	 * Center of given piece in the final position, assuming the origin at (0,0) on the upper left corner
	 * @param id
	 * @return center as a Point
	 * @throws MPJPException
	 */
	public Point getPieceStandardCenter​(int id) throws MPJPException {
		try {
			double piece_width = getPieceWidth();
			double piece_height = getPieceHeight();
			return new Point(piece_width/2 + getPieceColumn(id)*piece_width, piece_height/2 + getPieceRow(id)*piece_height);
		} catch(MPJPException e) { throw new MPJPException("Wrong ID",e); }
	}
	
	/**
	 * Locations of given piece in the final position, assuming the origin at (0,0) on the upper left corner
	 * @return map of IDs to locations
	 */
	public Map<Integer,Point> getStandardLocations() {
		Map<Integer,Point> map = new HashMap<Integer,Point>();
		
		for (int i = 0; i < getPieceCount(); i++) {
			try {
				map.put(i, new Point(getPieceStandardCenter​(i).getX(),getPieceStandardCenter​(i).getY()));
			} catch (MPJPException e) {
				e.printStackTrace();
			}
		}
		
		return map;
	}
	
	/**
	 * Set of pieces where a point might be located in a complete puzzle. Center of coordinates in to top left corner of the puzzle
	 * @param point
	 * @return
	 * @throws MPJPException
	 */
	public Set<Integer> getPossiblePiecesInStandarFor​(Point point) throws MPJPException { 
		Set<Integer> set = new HashSet<Integer>();
		int x = (int) (point.getX()/getPieceWidth());
		int y = (int) (point.getY()/getPieceHeight());
		int id = x+columns*y;
		set.add(id);
		
		int x1,y1;
		for (int i=-1;i<=1;i++) {
			for (int j=-1;j<=1;j++) {
				x1=x+i;
				y1=y+j;
				if (x1 >= 0 && x1 < columns && y1 >= 0 && y1 < rows) {
					id = x1+columns*y1;
					set.add(id);
				}
			}
		}
		return set;
	}
	
	// -- Setters --
	
	public void setRows(int rows) {
		this.rows = rows;
	}
	
	public void setColumns(int columns) {
		this.columns = columns;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public void setHeight(double height) {
		this.height = height;
	}
	
	// -- Auxiliary --
	
	/**
	 * A random point in the standard puzzle, with origin (0,0) at the upper left corner and bottom right corner a (width,height).
	 * @return point in standard puzzle
	 */
	public Point getRandomPointInStandardPuzzle() {
		return new Point(Math.random() * width, Math.random() * height);
	}
	
	/**
	 * Iterator over piece IDs, an integer from 0 (inclusive) to the number of pieces (exclusive), in ascending order.
	 */
	public Iterator<Integer> iterator() { 
		final int piece_count = getPieceCount();
		
		return (Iterator<Integer>) new Iterator<Integer>() {
			
			int count = 0;
			
			@Override
			public Integer next() {return count++;}
			
			@Override
			public boolean hasNext() {return count < piece_count;}
			
			@Override
			public void remove() {throw new UnsupportedOperationException();}
		};
	}
	
}
