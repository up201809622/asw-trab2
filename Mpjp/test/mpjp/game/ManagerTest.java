package mpjp.game;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import mpjp.game.PuzzleData.Puzzle;
import mpjp.shared.MPJPException;
import mpjp.shared.PieceStatus;
import mpjp.shared.PuzzleInfo;
import mpjp.shared.PuzzleLayout;
import mpjp.shared.PuzzleSelectInfo;
import mpjp.shared.PuzzleView;
import mpjp.shared.geom.Point;

/**
* Template for a test class on Manager - YOU NEED TO IMPLEMENTS THESE TESTS!
* 
*/
class ManagerTest extends PuzzleData {
	static Manager manager;
	
	List<String> ids = new ArrayList<>();
	
	/**
	 * Get the singleton instance for tests 
	 */
	@BeforeAll
	static void firstSetUp() {
		manager = Manager.getInstance();
		
	}
	
	/**
	 * Reset the singleton to its previous state
	 */
	@BeforeEach
	void setUp() {
		manager.reset();
	}
	
	/**
	 * GetIntance should return always the same instance RepeatedTest(value = 10)
	 */
	@RepeatedTest(value = 10)
	void testGetInstance() {
		Manager test= Manager.getInstance();
		System.out.println("manager: " + manager.toString());
		System.out.println("test: " + test.toString());
		//assertTrue( test == manager);
		assertEquals(test,manager);
	}

	/**
	 * Check if a set of cuttings with is, al least one, is available.
	 */
	@Test
	void testGetAvailableCuttings() {
		java.util.Set<java.lang.String> cuttings= manager.getAvailableCuttings();
		assertFalse(cuttings.isEmpty());
	}

	/**
	 * Check if images in test resources are available
	 */
	@Test
	void testGetAvailableImages() {
		java.util.Set<java.lang.String> images = manager.getAvailableImages();
		assertFalse(images.isEmpty());
	}

	/**
	 * Check if available workspace reflect those that where
	 * created so far, and map IDs to the correct type.   
	 * 
	 * @throws MPJPException if something unexpected happens
	 */
	@Test
	void testGetAvailableWorkspaces() throws MPJPException {
		
		
		Map<String, PuzzleSelectInfo> map = null;
		Set<String> keys = null;
		
		for(Puzzle puzzle: Puzzle.values()) {
			String id = manager.createWorkspace​(puzzle.getPuzzleInfo());
			ids.add(id);
			
			map = manager.getAvailableWorkspaces();
			
			assertNotNull(map,"instance expected");
			
			keys = map.keySet();
		
			assertEquals(ids.size(),keys.size(),"Invalid size");
			assertEquals(new HashSet<>(ids),keys,"Unexpected keys");
		
			PuzzleSelectInfo info = map.get(id);
			
			assertNotNull(info,"PuzzleSelectInfo expected");
		}
		
		
	}

	/**
	 * Check if workspaces created from an info return an ID
	 * and that IDs change even after a minimal delay  
	 *  
	 * @param puzzle with data for testing
	 * @throws InterruptedException on error during sleep 
	 * @throws MPJPException if something unexpected happens
	 */
	@ParameterizedTest
	@EnumSource(Puzzle.class)
	void testCreateWorkspace(Puzzle puzzle) throws InterruptedException, MPJPException {
		PuzzleInfo info = puzzle.getPuzzleInfo();
		Set<String> ids = new HashSet<>();
		
		for(int c=0; c < REPETIONS; c++) {
			Workspace workspace = new Workspace(info);
			String id = workspace.getId();
			assertNotNull(id,"String expected");
			
			assertFalse(ids.contains(id),"different IDs expected");
			ids.add(id);
			
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}		
		}
	}

	/**
	 * Check if piece selection using current layout.
	 * Using their locations it should retrieve either the
	 * same piece or one with a higher block id, if two or
	 * more pieces overlap
	 * 
	 * @param puzzle to test
	 * @throws MPJPException if an unexpected exception occurs
	 */
	@ParameterizedTest
	@EnumSource(Puzzle.class)
	void testSelectPiece(Puzzle puzzle) throws MPJPException {
		Workspace workspace = new Workspace(puzzle.getPuzzleInfo());
		PuzzleLayout puzzleLayout = manager.getCurrentLayout​(workspace.getId());
		Map<Integer, PieceStatus> pieces = puzzleLayout.getPieces();
		double radius = workspace.getSelectRadius();
		double delta = Math.sqrt(radius);
		
		for(Integer id: pieces.keySet()) {
			PieceStatus piece = pieces.get(id);
			Point point = piece.getPosition();
			Point near = new Point(point.getX()+getDelta(delta),point.getY()+getDelta(delta));
			Integer block = piece.getBlock();
			
			Integer selected = manager.selectPiece​(workspace.getId(),near);
			
			Integer selectedBlock = pieces.get(selected).getBlock();
			
			assertNotNull(selected,"Some piece selected");
			
			assertTrue(id == selected || selectedBlock > block, "At least a higher block expected");
		}
	}

	/**
	 * Check if placing the first piece (0) at the center raises no exception,
	 * but trying place a non existing piece does raise an exception.
	 * 
	 * @param puzzle to test
	 * @throws MPJPException if something unexpected happens
	 */
	@ParameterizedTest
	@EnumSource(Puzzle.class)
	void testConnect(Puzzle puzzle) throws MPJPException {
		Workspace workspace = new Workspace(puzzle.getPuzzleInfo());
		PuzzleLayout puzzleLayout = workspace.getCurrentLayout();
		PuzzleView puzzleView = workspace.getPuzzleView();
		int nPoints = puzzleLayout.getPieces().size();
		Point center = new Point(puzzleView.getWorkspaceWidth()/2,puzzleView.getWorkspaceHeight()/2);
		
		try {
 			workspace.connect​(0, center);
 	    } catch (Exception e) {
 	        // fail("Should not throw exception");
 	    } 
		
		assertAll(
				() -> assertThrows(MPJPException.class,
						() -> workspace.connect​(nPoints, center),
						"exception expected with an non existing piece id"),
				() -> assertThrows(MPJPException.class,
						() -> workspace.connect​(nPoints, center),
						"exception expected with a negative piece id")
				);
	}

	/**
	 * Check if puzzle view corresponds to given puzzle info
	 * 
	 * @param puzzle to test
	 * @throws MPJPException  if unexpected exceptions occurs
	 */
	@ParameterizedTest
	@EnumSource(Puzzle.class)
	void testGetPuzzleView(Puzzle puzzle) throws MPJPException {
		PuzzleInfo info = puzzle.getPuzzleInfo();
		Workspace workspace = new Workspace(info);
		PuzzleView puzzleView = workspace.getPuzzleView();
		
		assertNotNull(puzzleView,"puzzle view expected");
		assertEquals(info.getWidth(),puzzleView.getPuzzleWidth(),"wrong width");
		assertEquals(info.getHeight(),puzzleView.getPuzzleHeight(),"wrong height");
		assertEquals(info.getImageName(),puzzleView.getImage(),"wrong image");
	}

	/**
	 * Check if puzzle layout corresponds to given puzzle info,
	 * particularly in the number of pieces, and the initial
	 * layout should be unsolved. 
	 * 
	 * @param puzzle to test
	 * @throws MPJPException  if unexpected exceptions occurs
	 */
	@ParameterizedTest
	@EnumSource(Puzzle.class)
	void testGetCurrentLayout(Puzzle puzzle) throws MPJPException {
		PuzzleInfo info = puzzle.getPuzzleInfo();
		Workspace workspace = new Workspace(info);
		PuzzleLayout puzzleLayout = workspace.getCurrentLayout();
		int pieceCount = info.getRows() * info.getColumns();
		
		assertNotNull(puzzleLayout,"puzzleLayout expected");
		assertAll(
				() -> assertEquals(pieceCount,puzzleLayout.getPieces().size(),
						"unexpected #pieces"),
				() -> assertEquals(pieceCount,puzzleLayout.getBlocks().size(),
						"unexpected #blocks (initialy equal to #pieces)")
				);
	}

}
